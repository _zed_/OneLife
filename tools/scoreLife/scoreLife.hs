module Main where

import qualified Text.ParserCombinators.Parsec as P

import Control.Applicative
import Control.Monad
import System.Environment
import System.Exit

import Data.List as L
import qualified Data.Map.Strict as M

type PlayerID = Int

data Birth = Birth
    { birthTime :: Int
    , birthID :: PlayerID
    , birthHash :: String
    , birthSex :: Char
    , birthCoords :: (Int , Int)
    , birthParent :: Maybe PlayerID
    , birthPop :: Int
    , birthChain :: Int
    } deriving (Eq, Ord, Show)

data Death = Death
    { deathTime :: Int
    , deathID :: PlayerID
    , deathHash :: String
    , deathAge :: Float
    , deathSex :: Char
    , deathCoords :: (Int , Int)
    , deathReason :: String
    , deathPop :: Int
    } deriving (Eq, Ord, Show)

data Life = Life
    { lifeBirthTime :: Int
    , lifeDeathTime :: Int
    , lifeID :: PlayerID
    , lifeHash :: String
    , lifeDeathAge :: Float
    , lifeSex :: Char
    , lifeBirthCoords :: (Int , Int)
    , lifeDeathCoords :: (Int , Int)
    , lifeParent :: Maybe Int
    , lifeDeathReason :: String
    , lifeChildren :: [PlayerID]
    } deriving (Eq, Ord, Show)

parseInt :: P.CharParser () Int
parseInt = read <$> liftA2 (++) (P.option "" $ P.string "-") (P.many1 P.digit) 

parseFloat :: P.CharParser () Float
parseFloat = read <$> liftA2 (++) (P.option "" $ P.string "-") (P.many1 $ P.choice [P.char '.', P.digit] )

thenSpace :: P.CharParser a b -> P.CharParser a b
thenSpace = (<* P.optional (P.many1 $ P.char ' '))

parseBirth :: P.CharParser () Birth
parseBirth = do
        thenSpace $ P.char 'B'
        birthTime <- thenSpace parseInt
        birthID <- thenSpace parseInt
        birthHash <- thenSpace $ P.many P.hexDigit
        birthSex <- thenSpace $ P.oneOf "FM"
        birthCoords <- thenSpace $ do
            P.char '(' 
            x <- parseInt
            P.char ','
            y <- parseInt
            P.char ')'
            return (x,y)
        birthParent <- thenSpace $ do
            P.optional (P.string "noParent")
            P.optionMaybe $ P.string "parent=" >> parseInt
        birthPop <- thenSpace $ P.string "pop=" >> parseInt
        birthChain <- thenSpace $ P.string "chain=" >> parseInt
        return $ Birth birthTime birthID birthHash birthSex birthCoords birthParent birthPop birthChain

parseDeath :: P.CharParser () Death
parseDeath = do
        thenSpace $ P.char 'D'
        deathTime <- thenSpace parseInt
        deathID <- thenSpace parseInt
        deathHash <- thenSpace $ P.many P.hexDigit
        deathAge <- thenSpace $ P.string "age=" >> parseFloat
        deathSex <- thenSpace $ P.oneOf "FM"
        deathCoords <- thenSpace $ do
            P.char '(' 
            x <- parseInt
            P.char ','
            y <- parseInt
            P.char ')'
            return (x,y)
        deathReason <- thenSpace $ P.many1 $ P.choice [P.char '_', P.alphaNum]
        deathPop <- thenSpace $ P.string "pop=" >> parseInt
        return $ Death deathTime deathID deathHash deathAge deathSex deathCoords deathReason deathPop


parseBirthLine :: String -> Either P.ParseError Birth
parseBirthLine = P.parse parseBirth "(unknown)"

parseDeathLine :: String -> Either P.ParseError Death
parseDeathLine = P.parse parseDeath "(unknown)"

parseLines :: [String] -> ([Birth],[Death])
parseLines = parseFile' ([],[]) where
    parseFile' c [] = c
    parseFile' (bs,ds) (line:lines) =
        case parseBirthLine line of
            Right b -> parseFile' (b:bs,ds) lines
            Left e1 -> case parseDeathLine line of
                Right d -> parseFile' (bs,d:ds) lines
                Left e2 -> error $ "parse fail in " ++ show line ++":\n" ++ show e1 ++ "\n" ++ show e2

parseFile :: FilePath -> IO ([Birth],[Death])
parseFile f = (parseLines . lines) <$> readFile f


type IndexedLives = M.Map PlayerID Life
extractLives :: ([Birth],[Death]) -> IndexedLives
extractLives (bs,ds) = L.foldr processDeath (L.foldr processBirth M.empty bs) ds where
    processBirth :: Birth -> IndexedLives -> IndexedLives
    processBirth (Birth birthTime birthID birthHash birthSex birthCoords birthParent _ _) =
        -- |XXX: we use that the data is presorted such that children are born
        -- after their parents
        M.insert birthID (Life birthTime 0 birthID birthHash 0.0 birthSex birthCoords (0,0) birthParent "" [])
        . maybe id (M.adjust (\l -> l { lifeChildren = birthID : lifeChildren l })) birthParent 
    processDeath (Death deathTime deathID _ deathAge _ deathCoords deathReason _) =
        M.adjust (\l -> l { lifeDeathTime = deathTime, lifeDeathAge = deathAge
                , lifeDeathCoords = deathCoords, lifeDeathReason = deathReason })
            deathID

data ScoreInfo = ScoreInfo
    { scoreInfoID :: PlayerID
    , scoreInfoRelatedness :: Float
    , scoreInfoAge :: Float
    , scoreInfoScore :: Float
    } deriving (Eq, Ord, Show)
type Score = Float

-- |calculate scores for the relations of a player not born before the player,
-- each score being based on genetic relatedness and age at death.
-- All siblings are assumed to be full, so genetic relatedness is 2^{-d} for a
-- direct descendent/ancestor, and 2^{-(d-1)} for other relations, where d is
-- the distance in the maternal family tree.
scoreInfo :: IndexedLives -> PlayerID -> Maybe [ScoreInfo]
scoreInfo ls id = do
    cutoff <- lifeBirthTime <$> M.lookup id ls 
    let mnull :: (a -> [ScoreInfo]) -> Maybe a -> [ScoreInfo]
        mnull = maybe []
        fi = fromIntegral
        score' :: Int -> Bool -> [PlayerID] -> PlayerID -> [ScoreInfo]
        score' dist isAncestor excludeChildren id =
            scoreDescendents dist isAncestor excludeChildren id ++
            ((score' (dist+1) True [id]) `mnull`) `mnull` (lifeParent <$> M.lookup id ls)
        scoreDescendents :: Int -> Bool -> [PlayerID] -> PlayerID -> [ScoreInfo]
        scoreDescendents dist isAncestor excludeChildren id =
            scoreRelation dist id ++
            ((concat . map (scoreDescendents (dist + if isAncestor then 0 else 1) False [])
                . (\\ excludeChildren)) `mnull` (lifeChildren <$> M.lookup id ls))
        scoreRelation :: Int -> PlayerID -> [ScoreInfo]
        scoreRelation dist id =
            (\l -> 
                let relatedness = 2**(-(fi dist)) 
                    age = lifeDeathAge l
                in if lifeBirthTime l < cutoff || age <= 40 then []
                    else [ScoreInfo id relatedness age $ relatedness
                    -- alternative scoring mechanism - scale according to age
                    -- at death, 0 for age 20 up to 1 for age 60:
                    -- * ((lifeDeathAge l - 20) / 40)
                    ]) `mnull`
                M.lookup id ls
    return $ score' 0 False [] id

score :: IndexedLives -> PlayerID -> Maybe Float
score ls id = (sum . map scoreInfoScore) <$> scoreInfo ls id

main :: IO ()
main = do
    args <- getArgs
    let (args',full) = case args of 
            "--full":args' -> (args',True)
            _ -> (args,False)
    case args' of
        [logFile, idStr] | [(id,_)] <- reads idStr -> do
            lives <- extractLives <$> parseFile logFile
            infos <- maybe (putStrLn "Couldn't find that player" >> exitFailure) return (scoreInfo lives id)
            when full $ mapM_ print infos >> putStrLn ""
            print $ (sum . map scoreInfoScore) infos
            exitSuccess

        _ -> do
            prog <- getProgName
            putStrLn $ "usage: " ++ prog ++ "[--full] LOGFILE ID"
            exitFailure
