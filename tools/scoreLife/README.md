`scoreLife` is a simple tool for calculating a score for a life in OHOL.

# Scoring
The score is calculated as follows: each relation of the player who
is born on or after the birthsecond of the player, including the player
themself, contributes to the score if they live to at least 40. The
contribution of such a relation is its coefficient of relationship to the
player, i.e. the proportion of genetic material shared. For the purpose of
this calculation, we pretend that all births involve an invisible father,
that all siblings are full siblings, and that there is no incest. In other
words, the score contributed by a relation is 2^{-d} for a direct descendent,
and 2^{-(d-1)} for other relations, where d is the graph distance in the
maternal family tree.

# Compiling
These instructions assume a unix-like system. For anything else, you're on
your own.

Requirements:
    * the Glasgow Haskell compiler ghc
    * the Parsec library, which you can probably install with the command
      `cabal install parsec`.

You should then be able to make scoreLife with `make`.

# Usage
Obtain life log files from http://publicdata.onehouronelife.com/ .
You want the file for the day of the life you want to score, and perhaps for
the following day(s) if you want the score to factor in relations who lived
on those days. Concatenate these files into a single log file (with `cat`).

Find the player ID of the life you want to score; the name logs in the same
directory as the life logs may help with this, as may the user id hashes.

Then run `./scoreLife LOGFILE ID` to calculate the score for the life,
or `./scoreLife --full LOGFILE ID` to also give some information on the
relations who contribute to the score.
